<?php
/**
 * Created by PhpStorm.
 * User: bryce
 * Date: 3/26/16
 * Time: 7:59 PM
 */

namespace LaravelHubSpotOAuth;


use Carbon\Carbon;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Config;
use SevenShores\Hubspot\Factory as Factory;

class AuthenticationRefresh extends GuzzleClient
{

    /**
     * @var AuthenticationToken the authentication token this instance is refreshing
     */
    private $authenticationToken;

    /**
     * AuthenticationRefresh constructor.
     * @param AuthenticationToken $authenticationToken requires the authentication token to be passed in
     */
    public function __construct(AuthenticationToken $authenticationToken) {
        parent::__construct();

        $this->authenticationToken = $authenticationToken;
    }

    /**
     * Use this to check if the token needs to be refreshed right now
     *
     * @param $refreshRate int the rate at which we are refreshing the token
     * @return bool whether or not we currently need an update
     */
    public function needsRefresh($refreshRate) {

        $lastUpdate = Carbon::instance($this->authenticationToken->updated_at);
        $nextUpdate = $lastUpdate->addSeconds($refreshRate);

        return $nextUpdate->lt(Carbon::now());
    }

    /**
     * Use this to refresh the authentication token
     */
    public function refresh() {
        /*
        $res = $this->request('POST',Config::get('hubspot.auth_refresh_url'),
            ['form_params' =>
                ['refresh_token' =>  $this->authenticationToken->refresh_token,
                    'client_id'    => Config::get('hubspot.oauth_client_id'),
                    'client_secret'=> Config::get('hubspot.oauth_secret'),
                    'grant_type' => 'refresh_token'
                ]
            ]
        );

        $results = json_decode($res->getBody());

        $accessToken = $results->access_token;
        $refreshToken = $results->refresh_token;
        $expiresIn = $results->expires_in;
        */

        $hubspot = new Factory([
                'key'      => Config::get('hubspot.oauth_secret'),
                'oauth'    => false,
                'oauth2'   => true]
        );

        $response = $hubspot->oAuth2()->getTokensByRefresh(
            Config::get('hubspot.oauth_client_id'),
            Config::get('hubspot.oauth_secret'),
            $this->authenticationToken->refresh_token
        );

        $data = $response->data;
        $accessToken = $data->access_token;
        $refreshToken = $data->refresh_token;
        $expiresIn = $data->expires_in;

        $this->authenticationToken->updateToken($accessToken, $refreshToken, $expiresIn);
    }

}