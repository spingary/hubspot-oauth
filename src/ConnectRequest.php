<?php
/**
 * Created by PhpStorm.
 * User: bryce
 * Date: 3/26/16
 * Time: 6:04 PM
 */

namespace LaravelHubSpotOAuth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use SevenShores\Hubspot\Factory as Factory;

class ConnectRequest extends FormRequest
{

    /**
     * @return array all rules needed for this connect request
     */
    public function rules() {
        return [
            'code' => 'required'
        ];
        /*
                return [
                    'access_token' => 'required',
                    'refresh_token' => 'required',
                    'expires_in' => 'required'
                ];
        */
    }

    /**
     * Overrides default in order to allow all authorizations on this path
     * @return bool true
     */
    public function authorize() {
        return true;
    }

    /**
     * Use this to generate the correct login link for a user
     *
     * @param $returnURL string the return url after the user has logged in
     * @return string the generated login url
     */
    public static function generateLogInURL($returnURL) {
        $hubspot = Factory::create(Config::get('hubspot.oauth_secret'));
        $url = $hubspot->oAuth2()->getAuthUrl(Config::get('hubspot.oauth_client_id'),$returnURL,['content','files','crm.objects.contacts.read','crm.objects.contacts.write']);
        return ($url);
        /*return sprintf("%s?client_id=%s&portalId=%s&redirect_uri=%s&scope=blog-rw+offline",
        return sprintf("%s?client_id=%s&redirect_uri=%s&scope=content%%20files",
            Config::get('hubspot.auth_url'),
            Config::get('hubspot.oauth_client_id'),
            $returnURL);
        */
    }
}