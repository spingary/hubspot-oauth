<?php
/**
 * Created by PhpStorm.
 * User: bryce
 * Date: 3/26/16
 * Time: 10:38 PM
 */

namespace LaravelHubSpotOAuth;


use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Mail;

class AuthenticationRefreshCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:hubspot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if the HubSpot authentication is current. Attempts to update the access tokens if it is not';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $authenticationToken = AuthenticationToken::fetchCurrentAuthenticationToken();

        if ($authenticationToken) {

            $authenticationRefresh = new AuthenticationRefresh($authenticationToken);

            if ($authenticationRefresh->needsRefresh(Config::get('hubspot.auth_refresh_threshold'))) {

                try {
                    $authenticationRefresh->refresh();

                    $this->comment("HubSpot authentication updated properly.");

                    $rcpts = [];
                    if (Config::get('hubspot.email_log') != '')
                        $rcpts[] = Config::get('hubspot.email_log');
                    if (!empty($rcpts))
                    {
                        $msg = sprintf("HubSpot auth token refreshed successfully\nEnv: %s\nPortal ID: %s",env('APP_ENV'),Config::get('hubspot.portal_id'));
                        Mail::raw($msg, function($message) use($rcpts) {
                            $sbj = '[Pierce Admin] OAuth token refreshed successfully.';
                            $message->subject($sbj);
                            $message->from(Config::get('hubspot.email_from'));
                            $message->to($rcpts);
                        });
                    }

                } catch (Exception $e){

                    $this->comment("Unable to update HubSpot authentication.");

                    $rcpts = [];
                    if (Config::get('hubspot.email_log') != '')
                        $rcpts[] = Config::get('hubspot.email_log');
                    if (Config::get('hubspot.email_notify') != '')
                        $rcpts[] = Config::get('hubspot.email_notify');
                    if (!empty($rcpts))
                    {
                        $msg = sprintf("HubSpot auth token refresh failed!\nEnv: %s\nPortal ID: %s",env('APP_ENV'),Config::get('hubspot.portal_id'));
                        Mail::raw($msg, function($message) use($rcpts) {
                            $sbj = '[Pierce Admin] Failed refreshing oauth token!';
                            $message->subject($sbj);
                            $message->from(Config::get('hubspot.email_from'));
                            $message->to($rcpts);
                            if (Config::get('hubspot.email_notify_cc'))
                                $message->cc(Config::get('hubspot.email_notify_cc'));
                        });
                    }
                }

                return;
            }
            else {
                $this->comment("Authentication still valid.");
            }
        }
        else {
            $this->comment("No Valid authentication token.");

            $rcpts = [];
            if (Config::get('hubspot.email_log') != '')
                $rcpts[] = Config::get('hubspot.email_log');
            if (Config::get('hubspot.email_notify') != '')
                $rcpts[] = Config::get('hubspot.email_notify');
            if (!empty($rcpts))
            {
                $msg = sprintf("No valid HubSpot auth tokens condition encountered!\nEnv: %s\nPortal ID: %s",env('APP_ENV'),Config::get('hubspot.portal_id'));
                Mail::raw($msg, function($message) use($rcpts){
                    $sbj = '[Pierce Admin] No valid auth token!';
                    $message->subject($sbj);
                    $message->from(Config::get('hubspot.email_from'));
                    $message->to($rcpts);
                    if (Config::get('hubspot.email_cc'))
                        $message->cc(Config::get('hubspot.email_cc'));
                });
            }
        }
    }

}
