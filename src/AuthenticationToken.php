<?php

namespace LaravelHubSpotOAuth;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AuthenticationToken
 * @package App
 *
 * @property string access_token The token currently available for
 * @property string refresh_token The token used to refresh the existing access token
 * @property DateTime expires_at The time when the token expires
 * @property DateTime updated_at The time that this record was last updated
 */
class AuthenticationToken extends Model
{

    /**
     * @var array The date time fields to fill
     */
    protected $dates = ['expires_at', 'updated_at'];

    /**
     * Use this to update properties of an existing authentication token
     *
     * @param $accessToken string the access token that will be used for access
     * @param $refreshToken string the refresh token that will eventually be used to generate a new token
     * @param $expiresIn int How many seconds until the access token expires
     */
    public function updateToken($accessToken, $refreshToken, $expiresIn) {

        $dt = Carbon::now();

        $this->access_token = $accessToken;
        $this->refresh_token = $refreshToken;
        $this->expires_at = $dt->addSeconds($expiresIn)->toDateTimeString();

        $this->save();
    }

    /**
     * Use this to create a new authentication token
     *
     * @param $accessToken string the access token that will be used for access
     * @param $refreshToken string the refresh token that will eventually be used to generate a new token
     * @param $expiresIn int How many seconds until the access token expires
     * @return AuthenticationToken The newly created authentication token
     */
    public static function createToken($accessToken, $refreshToken, $expiresIn) {

        $authenticationToken = new AuthenticationToken();

        $authenticationToken->updateToken($accessToken, $refreshToken, $expiresIn);

        return $authenticationToken;
    }

    /**
     * Use this to get the current authentication token
     *
     * @return AuthenticationToken|bool The current valid authentication token, or false if there is none
     */
    public static function fetchCurrentAuthenticationToken() {

        $authenticationTokens = static::where("expires_at", ">", Carbon::now())->orderBy('expires_at', 'DESC')->get();

        return count($authenticationTokens) ? $authenticationTokens[0] : false;
    }
}
