<?php
/**
 * Created by PhpStorm.
 * User: bryce
 * Date: 3/26/16
 * Time: 8:45 PM
 */

namespace LaravelHubSpotOAuth;


use Closure;
use Exception;
//use Fungku\HubSpot\HubSpotService;
use SevenShores\Hubspot\Factory;
use Illuminate\Support\Facades\Config;
use Mail;

class AuthenticateHubSpotMiddleware
{

    /**
     * @var string default redirect url override this to set a different
     */
    protected $redirectTo = "/connect";

    /**
     * @var AuthenticationToken the current valid authentication token
     */
    private $authenticationToken = false;

    /**
     * @var AuthenticateHubSpotMiddleware the current instance of the middleware
     */
    private static $instance;

    /**
     * @var HubSpotService The current running service connect
     */
    private $hubSpotService = false;

    /**
     * Overriden so instance can be set
     *
     * AuthenticateHubSpotMiddleware constructor.
     */
    public function __construct()
    {
        static::$instance = $this;
    }

    /**
     * Override this in a subclass to allow a separate source for the authentication token
     *
     * @return bool|AuthenticationToken By default the global authentication token or false if there is none
     */
    public function loadAuthenticationToken() {
        return AuthenticationToken::fetchCurrentAuthenticationToken();
    }

    /**
     * Validates that there is a valid authentication token
     *
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public final function handle($request, Closure $next)
    {
        $this->authenticationToken = $this->loadAuthenticationToken();

        if ($this->authenticationToken) {

            $authenticationRefresh = new AuthenticationRefresh($this->authenticationToken);

            if ($authenticationRefresh->needsRefresh(Config::get('hubspot.auth_refresh_threshold'))) {

                try {
                    $authenticationRefresh->refresh();
                }
                catch (Exception $e) {

                    $rcpts = [];
                    if (Config::get('hubspot.email_log') != '')
                        $rcpts[] = Config::get('hubspot.email_log');
                    if (Config::get('hubspot.email_notify') != '')
                        $rcpts[] = Config::get('hubspot.email_notify');
                    if (!empty($rcpts))
                    {
                        $msg = sprintf("HubSpot auth token refresh failed!\nEnv: %s\nPortal ID: %s",env('APP_ENV'),Config::get('hubspot.portal_id'));
                        Mail::raw($msg, function($message) use($rcpts) {
                            $sbj = '[Pierce Admin] Failed refreshing oauth token!';
                            $message->subject($sbj);
                            $message->from(Config::get('hubspot.email_from'));
                            $message->to($rcpts);
                            if (Config::get('hubspot.email_notify_cc'))
                                $message->cc(Config::get('hubspot.email_notify_cc'));
                        });
                    }

                }
            }

            //$this->hubSpotService = HubSpotService::makeWithToken($this->authenticationToken->access_token);
            $this->hubSpotService = Factory::createWithToken($this->authenticationToken->access_token);
            // As of this version, the createWithToken method hard-sets client->oauth option to oauth1, so we have to change it.
            $this->hubSpotService->client->oauth = false;
            $this->hubSpotService->client->oauth2 = true;
            return $next($request);
        }
        else {

            $rcpts = [];
            if (Config::get('hubspot.email_log') != '')
                $rcpts[] = Config::get('hubspot.email_log');
            if (Config::get('hubspot.email_notify') != '')
                $rcpts[] = Config::get('hubspot.email_notify');
            if (!empty($rcpts))
            {
                $msg = sprintf("No valid HubSpot auth tokens condition encountered!\nEnv: %s\nPortal ID: %s",env('APP_ENV'),Config::get('hubspot.portal_id'));
                Mail::raw($msg, function($message) use($rcpts) {
                    $sbj = '[Pierce Admin] No valid auth token!';
                    $message->subject($sbj);
                    $message->from(Config::get('hubspot.email_from'));
                    $message->to($rcpts);
                    if (Config::get('hubspot.email_notify_cc'))
                        $message->cc(Config::get('hubspot.email_notify_cc'));
                });
            }
        }


        return redirect($this->redirectTo);
    }

    /**
     * Use this to retieve the current hub spot service
     *
     * @return bool|HubSpotService the current running hub spot service or false if there is none
     */
    public static function getHubSpotService() {

        if (static::$instance) {

            return static::$instance->hubSpotService;
        }

        return false;
    }

}
